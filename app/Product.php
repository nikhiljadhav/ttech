<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $table = 'product';
  //  Protected $primaryKey = "id";
    protected $fillable = [
          'order_no','product_description','product_size','product_quantity','product_drawing'
    ];
}
