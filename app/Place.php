<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
  protected $table = 'place';
  //  Protected $primaryKey = "id";
    protected $fillable = [
        'order_no','order_date','payment_terms','bank_details','dispatch_time'
        // ,'product_description','product_size','product_quantity','product_drawing'
    ];



    // public static function add(Request $input)
    // {
    //
    //     $data = $input['order_no'];
    //      foreach ($data as $key => $value) {
    //             $order = new Place();
    //             $order->order_no = $input['order_no'][$key];
    //             $order->order_date = $input['order_date'][$key];
    //             $order->product_description = $input['product_description'][$key];
    //             $order->product_size  = $input['product_size'][$key];
    //             $order->payment_terms = $input['payment_terms'][$key];
    //             $order->dispatch_time = $input['dispatch_time'][$key];
    //             // $student->test_name = $input['test_name'][$key];
    //             $order->save();
    //         }
    //         return ['success'=>true];
    // }


}
