<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Export_order extends Model
{
  protected $table = 'export_order';
  //  Protected $primaryKey = "id";
    protected $fillable = [
        'order_no','balance_payment','proforma_invoice','invoice','packing_list',
        'shipping_agent_name','vessel_details','bill_of_lading','clearance_document',
        'other_document','balance_due_date','status'
    ];
}
