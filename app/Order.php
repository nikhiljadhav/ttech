<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class Order extends Model
{
    //use Notifiable;
    protected $table='order';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'email','mobile','order_id','order_date','courier_id','courier_date','courier_details','order_status','username','password'
    // ];

    protected $fillable = [
      'order_id'
    ];


    public static function getRollNo()
    {
        $stdId= DB::table('users')->orderBy('id', 'desc')->first();

        if($stdId == null)
        {
            $roll_no='USER1';
            return $roll_no;
        }
        else{
        $id=$stdId->id;
        $count=(int)$id+1;
     $roll_no='USER'.$count;
        return $roll_no;
        }
    }



    public static function saveOrder(Request $request)
    {
        try {

        $order = new Order();

        // $order->name = $request->input('name');
        // $order->email = $request->input('email');
        // $order->mobile = $request->input('mobile');

        $order->order_id = $request->input('order_id');
        $name=$request->input('order_id');
        // $order->order_date = $request->input('order_date');
        // $order->courier_id=$request->input('courier_id');
        // $order->courier_date = $request->input('courier_date');
        // $order->courier_details = $request->input('courier_details');
        // $order->order_status = $request->input('order_status');
        $order->username = Order::getRollNo();

     $password = mt_rand(10000000, 99999999);
        $u_password=  $order->username;
        $order->password = $password;
        // dd($request);
        $order->save();


        $user = new User();
        $user->name =$name;
        // $user->email = $request->input('email');
        $user->username = $u_password;
      //  $user->name = $request->input('name');
        $user->password = bcrypt($password);
        $user->save();


            DB::commit();
            return ['success'=>true];

        } catch (\Exception $e) {

            DB::rollBack();

            return ['success'=>false,'errors'=>$e->getMessage()];

    }
    }


    public static function deleteOrder(Order $data)
       {
           \DB::table('orders')->where('order_id','=',$data->order_id)->delete();
           \DB::table('users')->where('username','=',$data->username)->delete();
       }

       public static function getOrderById(Request $request){
      try
      {


          if($request)
          {
              DB::beginTransaction();

              $get_table_data=DB::table('order')->find($request->id);
    return $get_table_data;
          }

      }
      catch (\Exception $e)
      {
          DB::rollBack();
      }
  }







}
