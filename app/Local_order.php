<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Local_order extends Model
{
  protected $table = 'local_order';
  //  Protected $primaryKey = "id";
    protected $fillable = [
        'order_no','balance_payment','proforma_invoice','invoice','packing_list',
        'company_name_transport','company_detail_transport','freight','lorry_receipt',
        'other_document','balance_due_date','status'
    ];
}
