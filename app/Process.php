<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Process extends Model
{
  protected $table = 'process';
  //  Protected $primaryKey = "id";
    protected $fillable = [
        'order_no','process_picture','process_packaging','quality_inspection','status'
    ];
}
