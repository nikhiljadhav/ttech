<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

     public function client_view()
     {
     return view('process.client_view');

     }



    public function index()
    {
            $user = Auth::user()->username;
            if ($user === "admin") {
              $orders = DB::table('order')->orderBy('created_at', 'desc')->paginate(5);
            return view('process.view_all_order', compact('orders'));
          }else
          {
            $order = DB::table('order')->where('username','=',$user)->first();

            $place=DB::table('place')->where('order_no','=',$order->order_id)->first();
              $process=DB::table('process')->where('order_no','=',$order->order_id)->first();
            $product=DB::table('product')->where('order_no','=',$order->order_id)->get();
            $export_order=DB::table('export_order')->where('order_no','=',$order->order_id)->first();
            $local_order=DB::table('local_order')->where('order_no','=',$order->order_id)->first();


            $status=DB::table('status')->where('value','=',$order->order_status)->first();
// dd($status);
            // return view('orders.user_order_view', compact('order','place','process','product','export_order','local_order','status'));

            return view('process.client_view', compact('order','place','process','product','export_order','local_order','status'));

          }


}


public function get_admin()
{
return view('process.admintemp');

}


}
