<?php

namespace App\Http\Controllers;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Laracasts\Flash\Flash;

class ProductController extends Controller
{
  public function get_product(Request $request)
    {

      $order = DB::table('order')->find($request->id);

        return view('process.get_product', compact('order'));
    }



    public function getBankDetail(Request $request)
    {

        $input = $request->all();
        $image = $request->file('product_drawing');
  //$request->file('upfile')->getClientOriginalName();
  //To get file name without extension
  //basename($request->file('upfile')->getClientOriginalName(), '.'.$request->file('upfile')->getClientOriginalExtension());

        $input['product_drawing'] = $image->getClientOriginalName();
        // . '.' . $image->getClientOriginalExtension();

        $destinationPath = public_path('/images/product_drawing');

        $image->move($destinationPath, $input['product_drawing']);


        return $input;
    }

    public function add_product(Request $request)
    {
        $validator = Validator::make($request->all(), [

          'product_description' => 'required',
          'product_size' => 'required',
          'product_quantity' => 'required',
           'product_drawing' => 'required|mimes:pdf,jpeg,png,jpg,gif,svg,txt|max:2048',

        ]);

        if ($validator->passes()) {

            $input = $this->getBankDetail($request);
            Product::create($input);
            // flash()->success('Product added successfully');
            $request->session()->flash('success', 'Product added successfully');
            return redirect()->back();
        }
        return redirect()->back()->withErrors($validator->errors())->withInput();
        // flash()->error($validator->errors()->first());
        return redirect()->back();
    }


}
