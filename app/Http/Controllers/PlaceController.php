<?php

namespace App\Http\Controllers;
use App\Place;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Laracasts\Flash\Flash;


class PlaceController extends Controller
{
// protected $dates = ['order_date'];
  // public function __construct()
  // {
  //     $this->middleware('auth');
  // }


  public function get_place_order(Request $request)
    {

      $order = DB::table('order')->find($request->id);

        return view('process.get_place', compact('order'));
    }

  public function getBankDetail(Request $request)
  {

      $input = $request->all();
//       $image = $request->file('product_drawing');
// //$request->file('upfile')->getClientOriginalName();
// //To get file name without extension
// //basename($request->file('upfile')->getClientOriginalName(), '.'.$request->file('upfile')->getClientOriginalExtension());
//
//       $input['product_drawing'] = $image->getClientOriginalName();
//       // . '.' . $image->getClientOriginalExtension();
//
//       $destinationPath = public_path('/images/product_drawing');
//
//       $image->move($destinationPath, $input['product_drawing']);
      $image1 = $request->file('bank_details');
      //$request->file('upfile')->getClientOriginalName();
      //To get file name without extension
      //basename($request->file('upfile')->getClientOriginalName(), '.'.$request->file('upfile')->getClientOriginalExtension());

      $input['bank_details'] =$image1->getClientOriginalName();
      //. '.' . $image1->getClientOriginalExtension();

      $destinationPath1 = public_path('/images/bank_details');

      $image1->move($destinationPath1, $input['bank_details']);

// $dt=$request->order_date;
//       $x=  $dt->toFormattedDateString();
//   $input['product_description'] =$x;



      return $input;
  }

  public function add_place_order(Request $request)
  {

    // $mytime = Carbon\Carbon::now();
  //  $dt = Carbon::now()->format('Y-m-d');
  //  $dt = new Carbon();
    //$before = $dt->subDay()->format('Y-m-d');
    $before=Carbon::yesterday()->format('Y-m-d');

   // dd($x);
      $validator = Validator::make($request->all(), [
        // 'product_drawing' => 'required|mimes:pdf,jpeg,png,jpg,gif,svg,txt|max:2048',
         'order_date' => 'required|date|after:'.$before,
        'payment_terms' => 'required',
        'dispatch_time' => 'required',
            'bank_details' => 'required|mimes:pdf,jpeg,png,jpg,gif,svg,txt|max:2048',
      ]);

      if ($validator->passes()) {

          $input = $this->getBankDetail($request);
          Place::create($input);
          flash()->success('Order placed successfully');
    return redirect('view_all_order');
          // return redirect()->back();
      }
        return redirect()->back()->withErrors($validator->errors())->withInput();
      // flash()->error($validator->errors()->first());
      // return redirect()->back();
  }

}
