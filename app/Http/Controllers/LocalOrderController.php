<?php

namespace App\Http\Controllers;
use App\Local_order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Laracasts\Flash\Flash;

class LocalOrderController extends Controller
{
  public function get_local_order(Request $request)
    {

      $order = DB::table('order')->find($request->id);
        return view('process.get_local', compact('order'));
    }



    public function getImagePath(Request $request)
    {

        $input = $request->all();

//$request->file('upfile')->getClientOriginalName();
//To get file name without extension
//basename($request->file('upfile')->getClientOriginalName(), '.'.$request->file('upfile')->getClientOriginalExtension());
        $image = $request->file('proforma_invoice');
        $input['proforma_invoice'] = $image->getClientOriginalName();
        $destinationPath = public_path('/images');
        $image->move($destinationPath, $input['proforma_invoice']);

        $image1 = $request->file('invoice');
        $input['invoice'] = $image1->getClientOriginalName();
        $destinationPath1 = public_path('/images');
        $image1->move($destinationPath1, $input['invoice']);

        $image2 = $request->file('packing_list');
        $input['packing_list'] = $image2->getClientOriginalName();
        $destinationPath2 = public_path('/images');
        $image2->move($destinationPath2, $input['packing_list']);

        $image3 = $request->file('lorry_receipt');
        $input['lorry_receipt'] = $image3->getClientOriginalName();
        $destinationPath3 = public_path('/images');
        $image3->move($destinationPath3, $input['lorry_receipt']);

        $image4 = $request->file('other_document');
        $input['other_document'] = $image4->getClientOriginalName();
        $destinationPath4 = public_path('/images');
        $image4->move($destinationPath4, $input['other_document']);

        return $input;
    }


    public function add_local_order(Request $request)
    {

        $before=Carbon::yesterday()->format('Y-m-d');
        $validator = Validator::make($request->all(), [
          //  'event_name' => 'required',
            //'event_description' => 'required',
            'balance_payment' => 'required',
            'freight' => 'required',
              'company_name_transport' => 'required',
              'company_detail_transport' => 'required',
              'balance_due_date' => 'required|date|after:'.$before ,

            'proforma_invoice' => 'required|mimes:pdf,txt,jpeg,png,jpg,gif,svg|max:2048',
              'invoice' => 'required|mimes:pdf,txt,jpeg,png,jpg,gif,svg|max:2048',
              'packing_list' => 'required|mimes:pdf,txt,jpeg,png,jpg,gif,svg|max:2048',
                'lorry_receipt' => 'required|mimes:pdf,txt,jpeg,png,jpg,gif,svg|max:2048',
                  'other_document' => 'required|mimes:pdf,txt,jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->passes()) {

            $input = $this->getImagePath($request);
          Local_order::create($input);
            flash()->success('Local order is added successfully');
                return redirect('view_all_order');
            // return redirect()->back();
        }
        return redirect()->back()->withErrors($validator->errors())->withInput();
      //  flash()->error($validator->errors()->first());
        return redirect()->back();
    }



}
