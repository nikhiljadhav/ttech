<?php

namespace App\Http\Controllers;
use App\Export_order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Laracasts\Flash\Flash;
class ExportOrderController extends Controller
{
  public function get_export_order(Request $request)
    {

      $order = DB::table('order')->find($request->id);
        return view('process.get_export', compact('order'));
    }

    public function getImagePath(Request $request)
    {

        $input = $request->all();

//$request->file('upfile')->getClientOriginalName();
//To get file name without extension
//basename($request->file('upfile')->getClientOriginalName(), '.'.$request->file('upfile')->getClientOriginalExtension());
        $image = $request->file('proforma_invoice');
        $input['proforma_invoice'] = $image->getClientOriginalName();
        $destinationPath = public_path('/images');
        $image->move($destinationPath, $input['proforma_invoice']);

        $image1 = $request->file('invoice');
        $input['invoice'] = $image1->getClientOriginalName();
        $destinationPath1 = public_path('/images');
        $image1->move($destinationPath1, $input['invoice']);

        $image2 = $request->file('packing_list');
        $input['packing_list'] = $image2->getClientOriginalName();
        $destinationPath2 = public_path('/images');
        $image2->move($destinationPath2, $input['packing_list']);

        $image3 = $request->file('vessel_details');
        $input['vessel_details'] = $image3->getClientOriginalName();
        $destinationPath3 = public_path('/images');
        $image3->move($destinationPath3, $input['vessel_details']);

        $image4 = $request->file('bill_of_lading');
        $input['bill_of_lading'] = $image4->getClientOriginalName();
        $destinationPath4 = public_path('/images');
        $image4->move($destinationPath4, $input['bill_of_lading']);

        $image5 = $request->file('clearance_document');
        $input['clearance_document'] = $image5->getClientOriginalName();
        $destinationPath5 = public_path('/images');
        $image5->move($destinationPath5, $input['clearance_document']);

        $image6 = $request->file('other_document');
        $input['other_document'] = $image6->getClientOriginalName();
        $destinationPath6 = public_path('/images');
        $image6->move($destinationPath6, $input['other_document']);

        return $input;
    }


    public function add_export_order(Request $request)
    {
      $before=Carbon::yesterday()->format('Y-m-d');
        $validator = Validator::make($request->all(), [
          //  'event_name' => 'required',
            //'event_description' => 'required',
          //  'product_description' => 'required',
            'balance_payment' => 'required',
            'shipping_agent_name' => 'required',
            'balance_due_date' => 'required|date|after:'.$before,
            'proforma_invoice' => 'required|mimes:pdf,txt,jpeg,png,jpg,gif,svg|max:2048',
              'invoice' => 'required|mimes:pdf,txt,jpeg,png,jpg,gif,svg|max:2048',
              'packing_list' => 'required|mimes:pdf,txt,jpeg,png,jpg,gif,svg|max:2048',
                'vessel_details' => 'required|mimes:pdf,txt,jpeg,png,jpg,gif,svg|max:2048',
                  'bill_of_lading' => 'required|mimes:pdf,txt,jpeg,png,jpg,gif,svg|max:2048',
                    'clearance_document' => 'required|mimes:pdf,txt,jpeg,png,jpg,gif,svg|max:2048',
                     // 'other_document' => 'required|mimes:pdf,txt,jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->passes()) {

            $input = $this->getImagePath($request);
            Export_order::create($input);
            flash()->success('Export order is added successfully');
                return redirect('view_all_order');
            // return redirect()->back();
        }
        // flash()->error($validator->errors()->first());
return redirect()->back()->withErrors($validator->errors())->withInput();
        return redirect()->back();
    }

}
