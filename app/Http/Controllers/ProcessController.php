<?php

namespace App\Http\Controllers;
use App\Process;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Laracasts\Flash\Flash;

class ProcessController extends Controller
{
  public function get_process_order(Request $request)
    {

        $order = DB::table('order')->find($request->id);
        return view('process.get_process', compact('order'));
    }

  public function getImagePath(Request $request)
  {
      $input = $request->all();
      $image = $request->file('process_picture');
//$request->file('upfile')->getClientOriginalName();
//To get file name without extension
//basename($request->file('upfile')->getClientOriginalName(), '.'.$request->file('upfile')->getClientOriginalExtension());
      $input['process_picture'] = $image->getClientOriginalName();
      //. '.' . $image->getClientOriginalExtension();
      $destinationPath = public_path('/images');
      $image->move($destinationPath, $input['process_picture']);

      $image1 = $request->file('process_packaging');
        $input['process_packaging'] =$image1->getClientOriginalName();
        //.  '.' . $image1->getClientOriginalExtension();

      $destinationPath1 = public_path('/images');

      $image1->move($destinationPath1, $input['process_packaging']);

      $image2 = $request->file('quality_inspection');
        $input['quality_inspection'] = $image2->getClientOriginalName();
      //  . '.' . $image2->getClientOriginalExtension();

      $destinationPath2 = public_path('/images');

      $image2->move($destinationPath2, $input['quality_inspection']);

      return $input;
  }


  public function add_process_order(Request $request)
  {
      $validator = Validator::make($request->all(), [
        'process_picture' => 'required|mimes:pdf,jpeg,png,jpg,gif,svg,txt|max:2048',
          'process_packaging' => 'required|mimes:pdf,jpeg,png,jpg,gif,svg,txt|max:2048',
            'quality_inspection' => 'required|mimes:pdf,jpeg,png,jpg,gif,svg,txt|max:2048',
      ]);

      if ($validator->passes()) {

          $input = $this->getImagePath($request);
          Process::create($input);
          flash()->success('Process details added successfully');
              return redirect('view_all_order');
          // return redirect()->back();
      }
        return redirect()->back()->withErrors($validator->errors())->withInput();
      // flash()->error($validator->errors()->first());
      // return redirect()->back();
  }

}
