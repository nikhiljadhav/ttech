<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Laracasts\Flash\Flash;

class OrderController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth');
  }


  public function get_order(){
    return view('process.order');
  }

public function get_status(Request $request){
  $order = Order::getOrderById($request);
  return view('process.change_status', compact('order'));
}

  public function change_status(Request $request){
    //   $order = Order::find($request->input('id'));
    // //   $order->name = $request->input('order_id');
    //    $order->order_status = $request->input('order_status');
    //    // dd($request);
    //     $order->save();
    //         Flash::success('Order status change successfully');
    // return redirect()->back();

  $order = Order::find($request->input('id'));
  $order->order_status = $request->input('order_status');
  $validator = Validator::make($request->all(),
        [

            'order_status' => 'required'
            ]);

            if ($validator->passes()) {

              $order->save();
                flash()->success('Order status change successfully');
                  return redirect('view_all_order');
            }
            return redirect()->back()->withErrors($validator->errors())->withInput();
            // flash()->error($validator->errors()->first());
            // return redirect()->back();



  }


  public function view_all_order(){

    $orders = DB::table('order')->orderBy('created_at', 'desc')->paginate(5);
  return view('process.view_all_order', compact('orders'));
        // return view('process.view_all_order', ['orders' => $orders]);
  }



    public function create_order(Request $request){

      $validator = Validator::make($request->all(),
          [

              'order_id' => 'required|numeric'
              ]);

      if ($validator->passes()) {
          $valid = true;
      } else {
          $valid = false;
      }
      if ($valid) {
          $result = Order::saveOrder($request);
          if ($result['success']) {
              Flash::success('Order added successfully');
              // return redirect()->back();
                return redirect('view_all_order');
          } else {
              flash()->error('Email id already exists, unable to add record');
              return redirect()->back()->withInput();
          }
      } else {
          return redirect()->back()->withErrors($validator->errors())->withInput();
      }




    }



public function get_feedback(){
  return view('orders.feedback');
}

  public function addOrders(Request $request)
   {

       $validator = Validator::make($request->all(),
           [
               'name' => 'required|alpha',
               'email' => 'required|email',
               'mobile' => 'required|numeric|regex:/[7-9]{1}[0-9]{9}/',
               'order_id' => 'required|numeric',
               'order_date' => 'required|date',
               'courier_id' => 'required',
               'courier_date' => 'required|date',
               'courier_details' => 'required|max:255',
               'order_status' => 'required',

               ]);

       if ($validator->passes()) {
           $valid = true;
       } else {
           $valid = false;
       }
       if ($valid) {
           $result = Order::saveOrder($request);
           if ($result['success']) {
               Flash::success('Order added successfully');
               return redirect()->back();
           } else {
               flash()->error('Email id already exists, unable to add record');
               return redirect()->back()->withInput();
           }
       } else {
           return redirect()->back()->withErrors($validator->errors())->withInput();
       }
   }


   public function view()
     {
         // try {
         //
         //         DB::beginTransaction();
                 $getOrderData = DB::table('orders')->orderBy('id', 'desc')->paginate(10);
                 return view('orders.view_orders', compact('getOrderData'));
         //     }
         //
         // } catch (\Exception $e) {
         //     DB::rollBack();
         // }
  }

  public function destroy(Request $request, Order $data)
      {
          try {


           Order::deleteOrder($data);
              $data->delete();
              flash()->success('Order is deleted successfully');
      return redirect()->back();
          } catch (\Exception $e) {
              flash()->error('Problem in deleting order');
    return redirect()->back();
          }
      }

      public function getById(Request $request)
        {
            $order = Order::getOrderById($request);
            return view('orders.update_order', compact('order'));
        }


        public function store(Request $request)
        {
            $order = Order::find($request->input('id'));

            $validator = Validator::make($request->all(),
                [
                  'name' => 'required|alpha',
                  'email' => 'required|email',

                  'mobile' => 'required|numeric|regex:/[7-9]{1}[0-9]{9}/',
                  'order_id' => 'required|numeric',
                  'order_date' => 'required|date',
                  'courier_id' => 'required',
                  'courier_date' => 'required|date',
                  'courier_details' => 'required|max:255',
                  'order_status' => 'required',
                ]);

            if ($validator->passes()) {
                $valid = true;
            } else {
                $valid = false;
            }
        if ($valid) {

         //        if ($request->has('del_logo')) {
         //            $filename = $student->profile_pic;
         //            File::delete(public_path("images/student/" . $filename));
         //            $student->profile_pic = "";
         //        }
         //
         //        if ($request->hasFile('picture')) {
         // $image = $request->file('picture');
         //            $filename = $student->profile_pic;
         //
         //            if (file_exists(public_path("images/student/" . $filename))) {
         //                File::delete(public_path("images/student/" . $filename));
         //            }
         //
         //            $student['profile_pic'] = time() . '.' . $image->getClientOriginalExtension();
         //
         //            $destinationPath = public_path('/images/student');
         //
         //            $image->move($destinationPath, $student['profile_pic']);
         //        }

         $order->name = $request->input('name');
         $order->email = $request->input('email');
         $order->mobile = $request->input('mobile');

         $order->order_id = $request->input('order_id');
         $order->order_date = $request->input('order_date');
         $order->courier_id=$request->input('courier_id');
         $order->courier_date = $request->input('courier_date');
         $order->courier_details = $request->input('courier_details');
         $order->order_status = $request->input('order_status');

                 try
              {

              //  User::changeStatusStudent($student);
                $order->save();
                flash()->success('Order updated successfully');
                return redirect()->back();

                  } catch (\Exception $e) {
            flash()->error('Problem in updating order, email is duplicate');
    //            $success="image uploaded successfully";

            return redirect()->back();
        }

             } else {
                return redirect()->back()->withErrors($validator->errors())->withInput();
            }
        }






}
