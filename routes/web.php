<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  //  return view('welcome');
      return view('auth.login');
});

Route::auth();
Auth::routes();

Route::get('/home','HomeController@index')->name('home');

Route::get('/client_view','HomeController@client_view')->name('client_view');

// Route::get('/get_order','HomeController@index')->name('get_order');
 Route::get('/create_new','OrderController@get_order')->name('create_new');

Route::post('/add_order','OrderController@addOrders')->name('add_order');

Route::get('/logout', function () {

    Auth::logout();
    return Redirect('/login');
}
);



Route::get('/get_admin','HomeController@get_admin')->name('get_admin');



Route::get('/feedback','OrderController@get_feedback')->name('feedback');

Route::get('/update_order/{id}','OrderController@getById');
Route::get('/view_order','OrderController@view')->name('view_order');
Route::put('/save_order', 'OrderController@store')->name('update_order');
Route::delete('/delete_order/{data}', 'OrderController@destroy');



//new_project starts here

// Route::get('/get_order','OrderController@get_order')->name('get_order');

Route::post('/create_order','OrderController@create_order')->name('create_order');
Route::get('/view_all_order','OrderController@view_all_order')->name('view_all_order');
Route::get('/get_status/{id}','OrderController@get_status')->name('get_status');
Route::put('/change_status','OrderController@change_status')->name('change_status');



Route::get('/get_product/{id}','ProductController@get_product')->name('get_product');
Route::post('/post_product','ProductController@add_product')->name('post_product');
Route::get('/get_export_order/{id}','ExportOrderController@get_export_order')->name('get_export_order');

Route::get('/get_local_order/{id}','LocalOrderController@get_local_order')->name('get_local_order');
Route::get('/get_place_order/{id}','PlaceController@get_place_order')->name('get_place_order');
Route::get('/get_process_order/{id}','ProcessController@get_process_order')->name('get_process_order');
Route::post('/post_export_order','ExportOrderController@add_export_order')->name('post_export_order');
Route::post('/post_local_order','LocalOrderController@add_local_order')->name('post_local_order');
Route::post('/post_place_order','PlaceController@add_place_order')->name('post_place_order');
Route::post('/post_process_order','ProcessController@add_process_order')->name('post_process_order');
