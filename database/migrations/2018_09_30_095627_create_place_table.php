<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('place', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_no')->nullable();
            $table->date('order_date')->nullable();
            $table->string('product_description')->nullable();
            $table->string('product_size')->nullable();
            $table->string('product_quantity')->nullable();
            $table->string('product_drawing')->nullable();
            $table->string('payment_terms')->nullable();
            $table->string('bank_details')->nullable();
            $table->string('dispatch_time')->nullable();
          $table->timestamps=false;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('place');
    }
}
