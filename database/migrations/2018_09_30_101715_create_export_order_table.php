<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExportOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('export_order', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_no')->nullable();
            $table->string('balance_payment')->nullable();
            $table->string('proforma_invoice')->nullable();
            $table->string('invoice')->nullable();
            $table->string('packing_list')->nullable();
            $table->string('shipping_agent_name')->nullable();
            $table->string('vessel_details')->nullable();
            $table->string('bill_of_lading')->nullable();
            $table->string('clearance_document')->nullable();
            $table->string('other_document')->nullable();
              $table->string('balance_due_date')->nullable();
            $table->string('status')->nullable();
            $table->timestamps=false;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('export_order');
    }
}
