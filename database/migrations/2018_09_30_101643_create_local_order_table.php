<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('local_order', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_no')->nullable();
          $table->string('balance_payment')->nullable();
          $table->string('proforma_invoice')->nullable();
          $table->string('invoice')->nullable();
            $table->string('packing_list')->nullable();
              $table->string('company_name_transport')->nullable();
                $table->string('company_detail_transport')->nullable();
                  $table->string('freight')->nullable();
                    $table->string('lorry_receipt')->nullable();
                      $table->string('other_document')->nullable();
                          $table->string('balance_due_date')->nullable();
          $table->string('status')->nullable();
            $table->timestamps=false;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('local_order');
    }
}
