@extends('layouts.app')

@section('content')
<script src="{{ asset("js/validation.js")}}"></script>
    <div class="page-content padding-0 container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
            <div class="col-lg-12 col-xlg-9">
                <div class="widget widget-shadow table-row">
                    <div class="widget-header padding-20">
                        <h3 class="widget-header blue-grey-700">Orders</h3>
                    </div>
                    <div class="alert" >
                  @include('flash::message')
                    </div>
                    @if(count($getOrderData) > 0)
                        <div class="widget-content bg-white table-responsive padding-20 padding-bottom-25">
                            <table class="table table-hover table-responsive table-striped" id="tblRecentAudits">
                                <tbody>
                                  <tr>
                                <th>No</th>
                                <th>Order Id</th>
                                <th>Client Name</th>
                                <th>Update Order</th>
                                <th>Delete Order</th>
                                <!-- <th>Add Documents</th> -->
                                <th>Order Status</th>
                                <!-- <th colspan="4" style="text-align:left;">Action</th> -->
                                </tr>
                                <?php
                                $i = 0;
                                ?>
                                @foreach($getOrderData as $getData)
                                    <tr>
                                        <td>
                                            <?php
                                            $i = $i + 1;
                                            echo $i;
                                            ?>
                                        </td>
                                        <td>
                                            {{ $getData->order_id }}
                                        </td>
                                         <td>
                                            {{ $getData->name }}
                                        </td>
                                        <td>
                                            <a href="{{ url('update_order',$getData->id) }}" class="btn btn-warning" style="text-decoration:none">Update</a>
                                        </td>
                                        <td>
                                            <form action="{{url('delete_order/'.$getData->id)}}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" id="save" class="btn btn-primary"  onclick="return ConfirmDelete();">
                                                    <i class="fa fa-btn fa-trash">&nbsp Delete</i>
                                                </button>
                                            </form>
                                        </td>
                                        <!-- <td>
                                            <a href="{{ url('student_view',$getData->id) }}" class="btn btn-info" style="text-decoration:none"><i class="fa fa-btn fa-eye"> View</i></a>
                                        </td> -->
                                        
                                        <!-- <td>Add Documents</td> -->
                                        <td>
                                          <!-- <select class="form-control" name="order_status" id="order_status" disabled> -->
                                            <select  name="order_status" id="order_status" disabled>
                                                                     <option value="">Select Status</option>
                                                                     <option value="first" {{ 'first' === $getData->order_status ? 'selected' : '' }}>Order Placed</option>
                                                                     <option value="second" {{ 'second' === $getData->order_status ? 'selected' : '' }}>Out For Delivery</option>
                                                                     <option value="third" {{ 'third' === $getData->order_status ? 'selected' : '' }}>Shipped</option>
                                                                     <option value="fourth" {{ 'fourth' === $getData->order_status ? 'selected' : '' }}>Delivered</option>
                                                                 </select>

                                        </td>
                                        <!-- <td>  </td> -->
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <ul class="pagination right-align">
                            {{ $getOrderData->render() }}
                        </ul>
                    @else
                        <h4>
                            <center>No orders to display</center>
                        </h4>
                    @endif

                </div>
            </div>

        </div>
    </div>
@endsection
