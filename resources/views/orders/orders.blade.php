
@extends('layouts.app')

@section('content')
<!-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                  order page
                </div>
            </div>
        </div>
    </div>
</div> -->

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <!-- <div class="card-header">{{ __('Register') }}</div> -->

                <div class="card-body">
                <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <!-- {{ Form::open( array('route'=>'add_order',
    'method'=>'POST',
    'class'=> 'form-horizontal',

    'autocomplete'=>'off', 'id'=>'insert_cust')) }}
   -->
    <form action="/add_order" method="POST" class="form-horizontal">
    <!-- {{ csrf_field() }}  -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="alert-important" >
        @include('flash::message')
  </div>
    <div class="row">
        <h2>Add Order</h2>
        <div class="col-md-6">
        </div>
    </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Name :</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? 'has-error' : '' }}" name="name" value="{{ old('name') }}"  autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail: </label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? 'has-error' : '' }}" name="email" value="{{ old('email') }}" >
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="mobile" class="col-md-4 col-form-label text-md-right">Mobile :</label>

                            <div class="col-md-6">
                                <input id="mobile" type="mobile" class="form-control{{ $errors->has('mobile') ? 'has-error' : '' }}" name="mobile" value="{{ old('mobile') }}">

                                @if ($errors->has('mobile'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="order_id" class="col-md-4 col-form-label text-md-right">Order Id :</label>

                            <div class="col-md-6">
                                <input id="order_id" type="order_id" class="form-control{{ $errors->has('order_id') ? 'has-error' : '' }}" name="order_id" value="{{ old('order_id') }}" >

                                @if ($errors->has('order_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('order_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>




                        <div class="form-group row">
                            <label for="order_date" class="col-md-4 col-form-label text-md-right">Order Date :</label>

                            <div class="col-md-6">
                                <input id="order_date" type="order_date" class="form-control{{ $errors->has('order_date') ? 'has-error' : '' }}" name="order_date" value="{{ old('order_date') }}" placeholder="yyyy-mm-dd">

                                @if ($errors->has('order_date'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('order_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="courier_id" class="col-md-4 col-form-label text-md-right">Courier Id :</label>

                            <div class="col-md-6">
                                <input id="courier_id" type="courier_id" class="form-control{{ $errors->has('courier_id') ? 'has-error' : '' }}" name="courier_id" value="{{ old('courier_id') }}">

                                @if ($errors->has('courier_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('courier_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="courier_date" class="col-md-4 col-form-label text-md-right">Courier Date :</label>

                            <div class="col-md-6">
                                <input id="courier_date" type="courier_date" class="form-control{{ $errors->has('courier_date') ? 'has-error' : '' }}" name="courier_date" value="{{ old('courier_date') }}" placeholder="yyyy-mm-dd">

                                @if ($errors->has('courier_date'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('courier_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="courier_details" class="col-md-4 col-form-label text-md-right">Courier Details :</label>

                            <div class="col-md-6">
                                <input id="courier_details" type="courier_details" class="form-control{{ $errors->has('courier_details') ? 'has-error' : '' }}" name="courier_details" value="{{ old('courier_details') }}">

                                @if ($errors->has('courier_details'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('courier_details') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="order_status" class="col-md-4 col-form-label text-md-right">Order Status :</label>

                            <div class="col-md-6">
                                <!-- <input id="order_status" type="order_status" class="form-control{{ $errors->has('order_status') ? 'has-error' : '' }}" name="order_status" value="{{ old('order_status') }}"> -->
                                {{ Form::select('order_status', [
                            '' => 'Select Status',
                            'first' => 'Order Placed',
                            'second' => 'Out For Delivery',
                            'third' => 'Shipped',
                            'fourth' => 'Delivered'
                            ],"",['class'=>'form-control',"id"=>'order_status']) }}
                                @if ($errors->has('order_status'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('order_status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    <!-- {{ __('Register') }} -->
                                    Add Order
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
