@extends('layouts.app')

@section('content')

<!-- <div class="page-content padding-0 container-fluid">
    <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="col-lg-12 col-xlg-9">
            <div class="widget widget-shadow table-row">
                <div class="widget-header padding-20">
                    <h3 class="widget-header blue-grey-700">Status</h3>
                </div> -->
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

               <div class="card-body">
<ul class="nav nav-tabs" style="width: 100%;">
    <li><a class="{{ $status->a }}" href="#aaa" data-toggle="tab">Order Placed  <span class="fa fa-check" style="text-align: -webkit-center;font-size:48px;color:yellowgreen;display:{{ 'active' === $status->a ? 'block' : 'none' }}"></span></a></li>
    <li class="{{ $status->b }}"><a href="#bbb" data-toggle="tab">Order is in process<span class="fa fa-check" style="text-align: -webkit-center;font-size:48px;color:yellowgreen;display:{{ 'active' === $status->b ? 'block' : 'none' }}"></span></a></li>
    <li class="{{ $status->c }}"><a href="#ccc" data-toggle="tab">Items are out for Delivery  <span class="fa fa-check" style="text-align: -webkit-center;font-size:48px;color:yellowgreen;display:{{ 'active' === $status->c ? 'block' : 'none' }}"></span></a></li>
    <li class="{{ $status->d }}"><a href="#ddd" data-toggle="tab">Delivered<span class="fa fa-check" style="text-align: -webkit-center;font-size:48px;color:yellowgreen;display:{{ 'active' === $status->d ? 'block' : 'none' }}"></span></a></li>
</ul>
<div class="tab-content" id="tabs">
    <div class="tab-pane" id="aaa">
      @if(count($place) > 0)

      {{ $place->order_date }}   <br>
       {{ $place->payment_terms }} <br>
      {{ $place->dispatch_time }} <br>

      @else
              <h4><center>No records to display</center></h4>
      @endif

      @if(count($product) > 0)


  @foreach($product as $getData)
<br>
{{ $getData->product_description  }} <br>
{{ $getData->product_size  }} <br>
{{$getData->product_quantity  }} <br>


@endforeach


      @else
              <h4><center>No records to display</center></h4>
      @endif

    </div>


    <div class="tab-pane" id="bbb">




      @if(count($process) > 0)
        Your items have been picked up by our courier partner.<br>

    {{ $process->order_no }}

      @else
              <h4><center>No records to display</center></h4>
      @endif


</div>

    <div class="tab-pane" id="ccc">


      @if(count($export_order) > 0)



{{  $export_order->balance_payment }}
{{  $export_order->shipping_agent_name }}
{{  $export_order->balance_due_date }}

      @else
              <h4><center>No records to display</center></h4>
      @endif

      @if(count($local_order) > 0)

{{  $local_order->balance_payment }}



      @else
              <h4><center>No records to display</center></h4>
      @endif


      Your item has been shipped.
    </div>



      <div class="tab-pane" id="ddd">



        <br>Your order has been delivered.
        <br>Please fill below feedback form
        <br>
        <br>
        <br>
        <div>
        <iframe src="https://docs.google.com/forms/d/e/1FAIpQLScRUaD_jdCSQgNuMNlwMMi4r_dJ-CVTBBEQQZR4i6dXexJ6sQ/viewform?embedded=true" width="640" height="806" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
        </div>

      </div>
</div>

</div>
</div>
</div>
</div>
</div>

@endsection
<!-- <script>
function myFunction() {
  var copyText = document.getElementById("myInput");
  copyText.select();
  document.execCommand("copy");

  document.getElementById('copied').innerHTML= copyText.value;
  //("Copied the text: " + );
}
</script> -->
