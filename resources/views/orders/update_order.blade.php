@extends('layouts.app')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    {{ Form::open( array('route'=>'update_order',
    'method'=>'POST',
    'class'=> 'form-horizontal'
    )) }}
    <div class="col-md-12">
        <div class="row">
            <h2>Order Details</h2>
            <div class="col-md-6">
            </div>
        </div>
          <div class="alert" >
        @include('flash::message')
          </div>
        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">Name :</label>

            <div class="col-md-6">
                <input id="name" type="text" class="form-control{{ $errors->has('name') ? 'has-error' : '' }}" name="name" value="{{ $order->name }}"  autofocus>

                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail: </label>
            <div class="col-md-6">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? 'has-error' : '' }}" name="email" value="{{ $order->email }}" >
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="mobile" class="col-md-4 col-form-label text-md-right">Mobile :</label>

            <div class="col-md-6">
                <input id="mobile" type="mobile" class="form-control{{ $errors->has('mobile') ? 'has-error' : '' }}" name="mobile" value="{{ $order->mobile }}">

                @if ($errors->has('mobile'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('mobile') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="order_id" class="col-md-4 col-form-label text-md-right">Order Id :</label>

            <div class="col-md-6">
                <input id="order_id" type="order_id" class="form-control{{ $errors->has('order_id') ? 'has-error' : '' }}" name="order_id" value="{{ $order->order_id }}" >

                @if ($errors->has('order_id'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('order_id') }}</strong>
                    </span>
                @endif
            </div>
        </div>




        <div class="form-group row">
            <label for="order_date" class="col-md-4 col-form-label text-md-right">Order Date :</label>

            <div class="col-md-6">
                <input id="order_date" type="order_date" class="form-control{{ $errors->has('order_date') ? 'has-error' : '' }}" name="order_date" value="{{ $order->order_date }}">

                @if ($errors->has('order_date'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('order_date') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="courier_id" class="col-md-4 col-form-label text-md-right">Courier Id :</label>

            <div class="col-md-6">
                <input id="courier_id" type="courier_id" class="form-control{{ $errors->has('courier_id') ? 'has-error' : '' }}" name="courier_id" value="{{ $order->courier_id }}">

                @if ($errors->has('courier_id'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('courier_id') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="courier_date" class="col-md-4 col-form-label text-md-right">Courier Date :</label>

            <div class="col-md-6">
                <input id="courier_date" type="courier_date" class="form-control{{ $errors->has('courier_date') ? 'has-error' : '' }}" name="courier_date" value="{{ $order->courier_date }}">

                @if ($errors->has('courier_date'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('courier_date') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="courier_details" class="col-md-4 col-form-label text-md-right">Courier Details :</label>

            <div class="col-md-6">
                <input id="courier_details" type="courier_details" class="form-control{{ $errors->has('courier_details') ? 'has-error' : '' }}" name="courier_details" value="{{ $order->courier_details }}">

                @if ($errors->has('courier_details'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('courier_details') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="order_status" class="col-md-4 col-form-label text-md-right">Order Status :</label>

            <div class="col-md-6">
                <!-- <input id="order_status" type="order_status" class="form-control{{ $errors->has('order_status') ? 'has-error' : '' }}" name="order_status" value="{{ $order->order_status }}"> -->

                <select class="form-control" name="order_status" id="order_status">
                                           <option value="">Select Status</option>
                                           <option value="first" {{ 'first' === $order->order_status ? 'selected' : '' }}>Order Placed</option>
                                           <option value="second" {{ 'second' === $order->order_status ? 'selected' : '' }}>Out For Delivery</option>
                                           <option value="third" {{ 'third' === $order->order_status ? 'selected' : '' }}>Shipped</option>
                                           <option value="fourth" {{ 'fourth' === $order->order_status ? 'selected' : '' }}>Delivered</option>
                                       </select>

                @if ($errors->has('order_status'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('order_status') }}</strong>
                    </span>
                @endif
            </div>
        </div>



        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <!-- <button type="submit" class="btn btn-primary"> -->
                  <input name="_method" type="hidden" value="PUT">
                  <input type="hidden" name="id" value="{{ $order->id }}">
                    <!-- {{ __('Register') }} -->
                      {{ Form::submit('Update Order', array('class' => 'btn btn-info')) }}
                </button>
            </div>
        </div>

    </div>

    {{ Form::close() }}
@endsection
