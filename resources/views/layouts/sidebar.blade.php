<div class="navbar-default sidebar" role="navigation">
              <div class="sidebar-nav navbar-collapse">
                  <ul class="nav" id="side-menu">
                      <li class="sidebar-search">
                          <div class="input-group custom-search-form">
                              <input type="text" class="form-control" placeholder="Search...">
                              <span class="input-group-btn">
                              <button class="btn btn-default" type="button">
                                  <i class="fa fa-search"></i>
                              </button>
                          </span>
                          </div>
                          <!-- /input-group -->
                      </li>
                      <li>
                          <a href="/view_all_order"><i class="fa fa-dashboard fa-fw"></i> All Orders</a>
                      </li>
                      <li>
                          <a href="/create_new"><i class="fa fa-dashboard fa-fw"></i> Create Order</a>

                      </li>
                      <li>
                          <a href="/logout"><i class="fa fa-dashboard fa-fw"></i> Logout</a>

                      </li>
                      
                  </ul>
              </div>
              <!-- /.sidebar-collapse -->
          </div>
          <!-- /.navbar-static-side -->
      </nav>




