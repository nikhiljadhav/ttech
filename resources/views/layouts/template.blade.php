<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tirath Tech @yield('title', 'Gujraat')</title>



    <!-- Bootstrap Core CSS -->
    <!-- <link href="global/tirath/css/bootstrap.min.css" rel="stylesheet"> -->
<link rel="stylesheet" href="{{ asset("global/tirath/bootstrap/css/bootstrap.min.css")}}">
    <!-- MetisMenu CSS -->
    <!-- <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet"> -->
<link rel="stylesheet" href="{{ asset("global/tirath/metisMenu/metisMenu.min.css")}}">
    <!-- Custom CSS -->
    <!-- <link href="../dist/css/sb-admin-2.css" rel="stylesheet"> -->
<link rel="stylesheet" href="{{ asset("global/tirath/dist/css/sb-admin-2.css")}}">
    <!-- Morris Charts CSS -->
    <!-- <link href="../vendor/morrisjs/morris.css" rel="stylesheet"> -->
<link rel="stylesheet" href="{{ asset("global/tirath/morrisjs/morris.css")}}">
    <!-- Custom Fonts -->
    <!-- <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->
<link rel="stylesheet" href="{{ asset("global/tirath/font-awesome/css/font-awesome.min.css")}}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
    .dataTables_paginate
    {
      display: none;
    }
    .dataTables_info{
        display: none;
    }
    .dataTables_length{
      display: none;
    }

    </style>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">SB Admin v2.0</a>
            </div>
            <!-- /.navbar-header -->

@include('layouts.sidebar')

@yield('page')

</div>
<!-- <link rel="stylesheet" href="{{ asset("global/tirath/css/bootstrap.min.css")}}"> -->
<!-- <script src="{{ asset("global/vendor/jquery/jquery.js")}}"></script> -->

<!-- <script src="../vendor/jquery/jquery.min.js"></script> -->
<script src="{{ asset('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js')}}"></script>
<script src="{{ asset("global/tirath/jquery/jquery.min.js")}}"></script>
    <!-- Bootstrap Core JavaScript -->
    <!-- <script src="../vendor/bootstrap/js/bootstrap.min.js"></script> -->
    <script src="{{ asset("global/tirath/bootstrap/js/bootstrap.min.js")}}"></script>



<script src="{{ asset("global/tirath/bootstrap/js/validation.js")}}"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <!-- <script src="../vendor/metisMenu/metisMenu.min.js"></script> -->
    <script src="{{ asset("global/tirath/metisMenu/metisMenu.min.js")}}"></script>

    <!-- Morris Charts JavaScript -->
    <!-- <script src="../vendor/raphael/raphael.min.js"></script> -->
    <script src="{{ asset("global/tirath/raphael/raphael.min.js")}}"></script>


    <!-- <script src="../vendor/morrisjs/morris.min.js"></script> -->
    <script src="{{ asset("global/tirath/morrisjs/morris.min.js")}}"></script>


    <!-- <script src="../data/morris-data.js"></script> -->
<script src="{{ asset("global/tirath/data/morris-data.js")}}"></script>

    <!-- Custom Theme JavaScript -->
    <!-- <script src="../dist/js/sb-admin-2.js"></script> -->

    <script src="{{ asset("global/tirath/datatables/js/jquery.dataTables.min.js")}}"></script>
        <script src="{{ asset("global/tirath/datatables-plugins/dataTables.bootstrap.min.js")}}"></script>
        <script src="{{ asset("global/tirath/datatables-responsive/dataTables.responsive.js")}}"></script>



<script src="{{ asset("global/tirath/dist/js/sb-admin-2.js")}}"></script>

<script>
$('div.alert .alert-success').not('.alert-important').delay(3000).fadeOut(350);
</script>
</body>
</html>
