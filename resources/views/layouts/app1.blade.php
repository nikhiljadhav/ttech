@extends('layouts.template')
@section('page')


  <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                      @yield('order')
                        </div>
                        <div class="panel-body">
                    @yield('content')
                </div>
              </div>
              </div>
              </div>
              </div>
              <script src="{{ asset('https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js')}}"></script>
              <script>
          $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
          </script>
@stop
