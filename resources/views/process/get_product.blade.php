@extends('layouts.app1')

@section('heads')

@endsection
@section('content')

@section('title', 'Product')
@section('order', 'Add Product')
<div class="row">
    <div class="col-lg-4">
	<meta name="csrf-token" content="{{ csrf_token() }}"/>
       <!-- {{ Form::open( array('route'=>'post_product',
    'method'=>'POST',
    'class'=> 'form-horizontal',
    'enctype'=>'multipart/form-data',
    'autocomplete'=>'off', 'role'=>'product')) }} -->



@if(Session::has('success'))
  <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                              {{ Session::get('success') }}
                            </div>
@endif
  <form role="form" action="/post_product" method="POST" enctype="multipart/form-data">
            <div class="form-group {{ $errors->has('order_no') ? 'has-error' : '' }}">
                <label>Order No</label>
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
      {{Form::input('text', 'order_no', $order->order_id , ['class'=>'form-control', 'id' => 'order_no',
                                               "maxlength"=>255,"disabled"=>"true"])}}
      {{Form::input('hidden', 'order_no', $order->order_id , ['class'=>'form-control', 'id' => 'order_no',
                                                                       "maxlength"=>255])}}
								<br>
                                @if ($errors->has('order_id'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('order_id') }}</strong>
                                    </p>
                                @endif
            </div>


			    <div class="form-group {{ $errors->has('product_description') ? 'has-error' : '' }}">
                <label>Enter Product Description</label>
         {{Form::input('text', 'product_description', '', ['class'=>'form-control', 'id' => 'product_description',
                                                 "placeholder"=>"Product Description", "maxlength"=>255])}}

								<br>
                                @if ($errors->has('product_description'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('product_description') }}</strong>
                                    </p>
                                @endif
            </div>
			 <div class="form-group {{ $errors->has('product_size') ? 'has-error' : '' }}">
                <label>Enter Product Description</label>
             {{Form::input('text', 'product_size', '', ['class'=>'form-control', 'id' => 'product_size',
                                                 "placeholder"=>"Product Size", "maxlength"=>255])}}
								<br>
                                @if ($errors->has('product_size'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('product_size') }}</strong>
                                    </p>
                                @endif
            </div>
			<div class="form-group {{ $errors->has('product_quantity') ? 'has-error' : '' }}">
                <label>Enter Product Quantity</label>
              {{Form::input('text', 'product_quantity', '', ['class'=>'form-control', 'id' => 'product_quantity',
                                                 "placeholder"=>"Product Quantity", "maxlength"=>255])}}
								<br>
                                @if ($errors->has('product_quantity'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('product_quantity') }}</strong>
                                    </p>
                                @endif
            </div>
			<div class="form-group  {{ $errors->has('product_drawing') ? 'has-error' : '' }}">
                                            <label>Upload Product Drawing</label>
                                            <input type="file" name="product_drawing" file-model="product_drawing">

                                                            @if ($errors->has('product_drawing'))
                                                                <p class="text-danger" role="alert">
                                                                    <strong>{{ $errors->first('product_drawing') }}</strong>
                                                                </p>
                                                            @endif


              </div>



			  {{ Form::submit('Add Product', ['class'=>'btn btn-primary']) }}
</form>
      <!-- {{ Form::close() }} -->
    </div>
  </div>





@endsection
