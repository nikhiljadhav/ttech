@extends('layouts.app1')

@section('heads')
<!-- <script src="{{ asset("js/validation.js")}}"></script> -->
@endsection

@section('content')

@section('title', 'Orders')
@section('order', 'Add Local/Export Order')

<meta name="csrf-token" content="{{ csrf_token() }}"/>

<div class="row">
    <div class="col-lg-4">

	<!-- <div class="form-group">
                <label>Select Order type</label>
     <select id="dropdown_change">
                      <option value="select">Select</option>
                          <option value="Export">Export</option>
                         <option value="local">Local</option>
                  </select>
								<br>

            </div> -->


            <div class="form-group">
                          <label>Select Order type</label>
                          <br>
                          <a href="{{ url('get_export_order',$order->id) }}" class="btn btn-primary" style="text-decoration:none" >Export Order</i></a>

                          <a href="{{ url('get_local_order',$order->id) }}" class="btn btn-success" style="text-decoration:none" > Local Order</i></a>
  <br>
                      </div>



  <form role="form" action="/post_local_order" method="POST" enctype="multipart/form-data"  id="form2" >
            <div class="form-group {{ $errors->has('order_no') ? 'has-error' : '' }}">
                <label>Order No</label>
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
    {{Form::input('text', 'order_no', $order->order_id , ['class'=>'form-control', 'id' => 'order_no',
                                                   "maxlength"=>255,"disabled"=>"true"])}}
          {{Form::input('hidden', 'order_no', $order->order_id , ['class'=>'form-control', 'id' => 'order_no',
                                                                           "maxlength"=>255])}}
								<br>
            </div>


			    <div class="form-group {{ $errors->has('balance_payment') ? 'has-error' : '' }}">
                <label>Select Balance Payment</label>

               {{ Form::select('balance_payment', [
                      '' => 'Select Payment',
                      'Before Dispatch' => ' % Before Dispatch',
                      'LC At Sight' => ' %  LC At Sight',
                      '30 Days' => ' %  30 Days'
                      ],"",['class'=>'form-control',"id"=>'balance_payment']) }}


								<br>
                                @if ($errors->has('balance_payment'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('balance_payment') }}</strong>
                                    </p>
                                @endif
            </div>
			 <div class="form-group {{ $errors->has('proforma_invoice') ? 'has-error' : '' }}">
                <label>Upload Proforma Invoice</label>
              <input type="file" name="proforma_invoice" file-model="proforma_invoice">
								<br>
                                @if ($errors->has('proforma_invoice'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('proforma_invoice') }}</strong>
                                    </p>
                                @endif
            </div>
			<div class="form-group {{ $errors->has('invoice') ? 'has-error' : '' }}">
                <label>Upload invoice</label>
              <input type="file" name="invoice" file-model="invoice">
								<br>
                                @if ($errors->has('invoice'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('invoice') }}</strong>
                                    </p>
                                @endif
            </div>
			<div class="form-group {{ $errors->has('packing_list') ? 'has-error' : '' }}">
                <label>Upload packing list</label>
              <input type="file" name="packing_list" file-model="packing_list">
								<br>
                                @if ($errors->has('packing_list'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('packing_list') }}</strong>
                                    </p>
                                @endif
            </div>
			<div class="form-group  {{ $errors->has('freight') ? 'has-error' : '' }}">
                                            <label>Enter freight</label>
                                              {{ Form::select('freight', [
                            '' => 'Select',
                            'To pay' => 'To Pay',
                            'Paid' => 'Paid',
                            ],"",['class'=>"form-control","id"=>'freight']) }}
                                                            @if ($errors->has('freight'))
                                                                <p class="text-danger" role="alert">
                                                                    <strong>{{ $errors->first('freight') }}</strong>
                                                                </p>
                                                            @endif
              </div>
               <div class="form-group {{ $errors->has('company_name_transport') ? 'has-error' : '' }}">
                <label>Enter transport company name</label>
               {{Form::input('text', 'company_name_transport','', ['class'=>'form-control', 'id' => 'company_name_transport',
                                                         "placeholder"=>"transport company name", "maxlength"=>255])}}
								<br>
                                @if ($errors->has('company_name_transport'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('company_name_transport') }}</strong>
                                    </p>
                                @endif
            </div>
			  <div class="form-group {{ $errors->has('company_detail_transport') ? 'has-error' : '' }}">
                <label>Enter transport company details</label>
               {{Form::input('text', 'company_detail_transport','', ['class'=>'form-control', 'id' => 'company_detail_transport',
                                                         "placeholder"=>"transport company details", "maxlength"=>255])}}
								<br>
                                @if ($errors->has('company_detail_transport'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('company_detail_transport') }}</strong>
                                    </p>
                                @endif
            </div>
			<div class="form-group {{ $errors->has('lorry_receipt') ? 'has-error' : '' }}">
                <label>Upload Lorry Receipt</label>
              <input type="file" name="lorry_receipt" file-model="lorry_receipt">
								<br>
                                @if ($errors->has('lorry_receipt'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('lorry_receipt') }}</strong>
                                    </p>
                                @endif
            </div>
			<div class="form-group {{ $errors->has('other_document') ? 'has-error' : '' }}">
                <label>Upload Other Documents</label>
                <input type="file" name="other_document" file-model="other_document">
								<br>
                                @if ($errors->has('other_document'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('other_document') }}</strong>
                                    </p>
                                @endif
            </div>
			<div class="form-group {{ $errors->has('balance_due_date') ? 'has-error' : '' }}">
                <label>Enter Balance Payment Due Date </label>
                {{Form::input('text', 'balance_due_date', '', ['class'=>'form-control', 'id' => 'balance_due_date',
                                                   "placeholder"=>"Balance Payment Due Date", "maxlength"=>255])}}
								<br>
                                @if ($errors->has('balance_due_date'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('balance_due_date') }}</strong>
                                    </p>
                                @endif
            </div>




			  {{ Form::submit('Local Order', ['class'=>'btn btn-primary']) }}
</form>

    </div>
  </div>





@endsection
