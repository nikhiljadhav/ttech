@extends('layouts.app1')

@section('heads')
<script src="{{ asset("js/validation.js")}}"></script>
<style>
@media (min-width: 768px)
{
#page-wrapper {
  position: inherit;
  margin: 0 0 0 250px;
  padding: 0 12px !important;

}
}
</style>
@endsection

@section('content')

@section('title', 'Orders')
@section('order', 'All Orders')



<!-- <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Tables</h1>
                </div>
                            </div> -->
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
    @include('flash::message')

                    @if(count($orders) > 0)
                    <!-- <label>Search:<input id="myInput" type="search"></label> -->
                            <table width="100%" class="table table-striped table-bordered table-hover responsive" id="example">
                                <thead>

                                    <tr>
                                       <!-- <th>No</th> -->
                                <th>ORDER NO</th>
                                <th class="min-tablet">ORDER STATUS</th>
                                <th class="min-tablet">PLACE ORDER</th>
                                <th class="min-tablet">ADD PRODUCT</th>
                                <th class="min-tablet">IN PROCESS</th>
                                <th class="min-tablet">OUT FOR DELIVERY</th>
                                <th class="min-tablet">CHANGE STATUS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </td>
                                @foreach($orders as $getData)
                                    <tr>

                                        <td>
                                        {{  $getData->order_id }}
                                          </td>
                                          <td>
                                            <select class="form-control" name="order_status" id="order_status" disabled>
                                            <option value="">Select Status</option>
                                          <option value="first" {{ 'first' === $getData->order_status ? 'selected' : '' }}>Order Placed</option>
                                          <option value="second" {{ 'second' === $getData->order_status ? 'selected' : '' }}>Order is in Process</option>
                                          <option value="third" {{ 'third' === $getData->order_status ? 'selected' : '' }}>Out For Delivery</option>
                                          <option value="fourth" {{ 'fourth' === $getData->order_status ? 'selected' : '' }}>Delivered</option>
                                          </select>
                                          </td>
                                         <td>
                                           <a href="{{ url('get_place_order',$getData->id) }}" class="btn btn-outline btn-link" style="text-decoration:none" > Place</i></a>

                                            </td>
                                            <td>
                                              <a href="{{ url('get_product',$getData->id) }}" class="btn btn-outline btn-link" style="text-decoration:none" >Add product</i></a>
                                            </td>
                                        <td>
                                          <a href="{{ url('get_process_order',$getData->id) }}" class="btn btn-outline btn-link" style="text-decoration:none" >in process</i></a>

                                        </td>
                                         <td>
                                           <a href="{{ url('get_local_order',$getData->id) }}" class="btn btn-outline btn-link" style="text-decoration:none" > out for delivery</i></a>
                                          <!-- <a href="{{ url('change_status',$getData->order_id) }}" class="btn btn-warning" style="text-decoration:none" ><i class="fa fa-btn fa-recycle"> Update</i></a> -->
                                        </td>
                                        <td>
                                        <a href="{{ url('get_status',$getData->id) }}" class="btn btn-outline btn-link" style="text-decoration:none" >Change status</i></a>
                                      </td>

                                    </tr>
                                @endforeach


                                </tbody>
                            </table>
							</div>

</div>

              <div class="row">
<div class="col-xs-6 col-sm-3">
<!-- <div class="dataTables_info" id="dataTables-example_info" role="status" aria-live="polite">Showing 31 to 40 of 57 entries -->
<!-- </div> -->
</div>
<div class="col-xs-6 col-sm-3">
<!-- <div class="dataTables_info" id="dataTables-example_info" role="status" aria-live="polite">Showing 31 to 40 of 57 entries -->
<!-- </div> -->
</div>
<div class="col-xs-6 col-sm-3">
<!-- <div class="dataTables_info" id="dataTables-example_info" role="status" aria-live="polite">Showing 31 to 40 of 57 entries -->
<!-- </div> -->
</div>
<!-- </div> -->
  <!-- <div class="row"> -->
<div class="col-xs-6 col-sm-3">
<!-- <div class="dataTables_paginate paging_simple_numbers" id="dataTables-example_paginate"> -->
<ul class="pagination right-align">
                              {{ $orders->render() }}
                            </ul>
                            @else
                       <h4><center>No records to display</center></h4>
                   @endif
  </div>


</div>





@endsection
