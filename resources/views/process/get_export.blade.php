@extends('layouts.app1')

@section('heads')
<!-- <script src="{{ asset("js/validation.js")}}"></script> -->
@endsection

@section('content')

@section('title', 'Orders')
@section('order', 'Add Local/Export Order')

<meta name="csrf-token" content="{{ csrf_token() }}"/>

<div class="row">
    <div class="col-lg-4">

	<!-- <div class="form-group">
                <label>Select Order type</label>
     <select id="dropdown_change">
                      <option value="select">Select</option>
                          <option value="Export">Export</option>
                         <option value="local">Local</option>
                  </select>
								<br>

            </div> -->



            <div class="form-group">
                          <label>Select Order type</label>
                          <br>
                          <a href="{{ url('get_export_order',$order->id) }}" class="btn btn-primary" style="text-decoration:none" >Export Order</i></a>

                          <a href="{{ url('get_local_order',$order->id) }}" class="btn btn-success" style="text-decoration:none" > Local Order</i></a>
  <br>

                      </div>



 <form role="form" action="/post_export_order" method="POST" enctype="multipart/form-data" id="form1" >
            <div class="form-group {{ $errors->has('order_no') ? 'has-error' : '' }}">
                <label>Order No</label>
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                       {{Form::input('text', 'order_no', $order->order_id , ['class'=>'form-control', 'id' => 'order_no',
                                                                "maxlength"=>255,"disabled"=>"true"])}}
                       {{Form::input('hidden', 'order_no', $order->order_id , ['class'=>'form-control', 'id' => 'order_no',
                                                                                        "maxlength"=>255])}}
								<br>
            </div>


			    <!-- <div class="form-group {{ $errors->has('product_description') ? 'has-error' : '' }}">
                <label>Enter Product Description</label>
         {{Form::input('text', 'product_description', '', ['class'=>'form-control', 'id' => 'product_description',
                                                 "placeholder"=>"Product Description", "maxlength"=>255])}}

								<br>
                                @if ($errors->has('product_description'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('product_description') }}</strong>
                                    </p>
                                @endif
            </div> -->
			 <div class="form-group {{ $errors->has('balance_payment') ? 'has-error' : '' }}">
                <label>Select Balance Payment</label>
                {{ Form::select('balance_payment', [
                       '' => 'Select Payment',
                       'Before Dispatch' => ' % Before Dispatch',
                       'LC At Sight' => ' %  LC At Sight',
                       '30 Days' => ' %  30 Days'
                       ],"",['class'=>'form-control',"id"=>'balance_payment']) }}

            		<br>
                                @if ($errors->has('balance_payment'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('balance_payment') }}</strong>
                                    </p>
                                @endif
            </div>
			<div class="form-group {{ $errors->has('proforma_invoice') ? 'has-error' : '' }}">
                <label>Enter Proforma Invoice</label>
  <input type="file" name="proforma_invoice" file-model="proforma_invoice">
          			<!-- <br> -->
                                @if ($errors->has('proforma_invoice'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('proforma_invoice') }}</strong>
                                    </p>
                                @endif
            </div>
			<div class="form-group  {{ $errors->has('invoice') ? 'has-error' : '' }}">
                                            <label>Upload Invoice</label>
                                              <input type="file" name="invoice" file-model="invoice">
                                                            @if ($errors->has('invoice'))
                                                                <p class="text-danger" role="alert">
                                                                    <strong>{{ $errors->first('invoice') }}</strong>
                                                                </p>
                                                            @endif


              </div>
              <div class="form-group  {{ $errors->has('packing_list') ? 'has-error' : '' }}">
                  <label>Upload Packing list</label>
            <input type="file" name="packing_list" file-model="packing_list">
                  @if ($errors->has('packing_list'))
                        <p class="text-danger" role="alert">
                <strong>{{ $errors->first('packing_list') }}</strong>
              </p>
              @endif
              </div>
              <div class="form-group  {{ $errors->has('shipping_agent_name') ? 'has-error' : '' }}">
                  <label>Enter Shipping Agent Name</label>
                  {{Form::input('text', 'shipping_agent_name', '', ['class'=>'form-control', 'id' => 'shipping_agent_name',
                                                             "placeholder"=>"Shipping Agent Name", "maxlength"=>255])}}
                  @if ($errors->has('shipping_agent_name'))
                        <p class="text-danger" role="alert">
                <strong>{{ $errors->first('shipping_agent_name') }}</strong>
              </p>
              @endif
              </div>
              <div class="form-group  {{ $errors->has('vessel_details') ? 'has-error' : '' }}">
                  <label>Upload Vessel Details</label>
            <input type="file" name="vessel_details" file-model="vessel_details">
                  @if ($errors->has('vessel_details'))
                        <p class="text-danger" role="alert">
                <strong>{{ $errors->first('vessel_details') }}</strong>
              </p>
              @endif
              </div>
              <div class="form-group  {{ $errors->has('bill_of_lading') ? 'has-error' : '' }}">
                  <label>Upload Bill of lading</label>
                    <input type="file" name="bill_of_lading" file-model="bill_of_lading">
                  @if ($errors->has('bill_of_lading'))
                        <p class="text-danger" role="alert">
                <strong>{{ $errors->first('bill_of_lading') }}</strong>
              </p>
              @endif
              </div>
              <div class="form-group  {{ $errors->has('clearance_document') ? 'has-error' : '' }}">
                  <label>Upload Clearance Document</label>
                  <input type="file" name="clearance_document" file-model="clearance_document">
                  @if ($errors->has('clearance_document'))
                        <p class="text-danger" role="alert">
                <strong>{{ $errors->first('clearance_document') }}</strong>
              </p>
              @endif
              </div>
              <div class="form-group  {{ $errors->has('other_document') ? 'has-error' : '' }}">
                  <label>Upload other Document</label>
                <input type="file" name="other_document" file-model="other_document">
                  @if ($errors->has('other_document'))
                        <p class="text-danger" role="alert">
                <strong>{{ $errors->first('other_document') }}</strong>
              </p>
              @endif
              </div>
              <div class="form-group  {{ $errors->has('balance_due_date') ? 'has-error' : '' }}">
                  <label>Enter Balance Due Date</label>
                  {{Form::input('text', 'balance_due_date', '', ['class'=>'form-control', 'id' => 'balance_due_date',
                                                                 "placeholder"=>"Balance Payment Due Date ", "maxlength"=>255])}}
                  @if ($errors->has('balance_due_date'))
                        <p class="text-danger" role="alert">
                <strong>{{ $errors->first('balance_due_date') }}</strong>
              </p>
              @endif
              </div>

			  {{ Form::submit('Export Order', ['class'=>'btn btn-primary']) }}
</form>





    </div>
  </div>





@endsection
