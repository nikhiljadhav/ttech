<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href='https://fonts.googleapis.com/css?family=Droid+Serif|Open+Sans:400,700' rel='stylesheet' type='text/css'>

	<!-- <link rel="stylesheet" href="global/client/css/reset.css"> -->
   <!-- CSS reset -->
  <link rel="stylesheet" href="{{ asset("global/client/css/reset.css")}}">

	<!-- <link rel="stylesheet" href="global/client/css/style.css">  -->
    <link rel="stylesheet" href="{{ asset("global/client/css/style.css")}}">
  <!-- Resource style -->
	<!-- <link rel="stylesheet" href="global/client/css/demo.css"> -->
  <link rel="stylesheet" href="{{ asset("global/client/css/demo.css")}}">
   <!-- Demo style -->
<style>
.proc{
border-collapse: collapse !important;
width: 100%;
border:0px !important;
}
.cd-nugget-info a
{
color:#ffffff !important;
}
body
{
	color: #04111d !important;
}
table {
    border-collapse: collapse;
 width: 100%;
}
td,th,table
{
	  border: 1px solid sandybrown;
}

td,th
{
text-align: center;
	 vertical-align: middle;
	 height: 70px;
}

@media only screen and (min-width: 1170px)
{
header {
    height: 100px !important;
    line-height: 300px;
}

@media only screen and (max-width: 600px)
{
header {
    height: 100px !important;
    line-height: 300px;
}

}
</style>
	<title>Tirath Tech</title>
</head>
<body>
	<header>
		<div class="cd-nugget-info">
			<a href="/logout">
				<span>
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="16px" height="16px" viewBox="0 0 16 16" style="enable-background:new 0 0 16 16;" xml:space="preserve">
						<style type="text/css">
							.cd-nugget-info-arrow{fill:#383838;}
						</style>
						<polygon class="cd-nugget-info-arrow" points="15,7 4.4,7 8.4,3 7,1.6 0.6,8 0.6,8 0.6,8 7,14.4 8.4,13 4.4,9 15,9 "/>
					</svg>
				</span>
			Logout
			</a>
			<br>
				<h1>ORDER NO. {{ $order->order_id  }}</h1>
		</div> <!-- cd-nugget-info -->

	</header>

	<section class="cd-timeline js-cd-timeline">
		<div class="cd-timeline__container">
			<div class="cd-timeline__block js-cd-block">
				<div class="cd-timeline__img cd-timeline__img--picture js-cd-img">
					<img src="global/client/img/placeholder (1).svg" alt="Picture">
				</div>

				<div class="cd-timeline__content js-cd-content">
					<h2>Order Placed</h2>
					<p>
            @if(count($place) > 0)
        1.  Order Date:   {{ $place->order_date }}   <br>
        2.  Selected Payment Term:   {{ $place->payment_terms }} <br>
        3.  Dispatch Time *:    {{ $place->dispatch_time }} Days<br>

				@if(count($product) > 0)

				<table>
					<tr>
						<th>Product Desc</th>
						<th>Product Size</th>
							 <th>Product Quantity</th>
					</tr>

			@foreach($product as $getData)
			<tr>
				<td>{{ $getData->product_description  }}</td>
				<td>  {{ $getData->product_size  }}</td>
					<td> {{$getData->product_quantity  }} </td>
			</tr>
			@endforeach
</table>

				@else
								<h4><center>Oops!!  No products to display</center></h4>
				@endif


<br>
<p>
*The estimated dispatch time given is on the basis of the
time given to us by our Vendors of various supplies and
then the time required by our team to manage those supplies
 to successfully provide you with your product. The time
  given by our vendors is beyond our managerial capabilities.
	Its only the time required by our team that comes under our
	 managerial control.So any change in the dispatch time shall be
	  notified to you with a proper reasoning.

</p>
            @else
                    <h4><center>No records to display</center></h4>
            @endif




          </p>
					<span class="cd-timeline__date">
					@if(count($place) > 0)
				{{
				\Carbon\Carbon::parse($place->order_date)->toFormattedDateString()

					}}
					@endif
					</span>
				</div>
			</div>

			<div class="cd-timeline__block js-cd-block">
				<div class="cd-timeline__img cd-timeline__img--movie js-cd-img">
					<img src="global/client/img/placeholder (2).svg" alt="Movie">
				</div>

				<div class="cd-timeline__content js-cd-content">
					<h2>Order is in Process</h2>
					<p>
            @if(count($process) > 0)
            Please download below documents :<br>
							<table class="proc">
							  <tr>
							    <th>Picture</th>
							    <th>Packaging</th>
							<th>Quality inspection Document</th>
							  </tr>
							  <tr>
							    <td><a href="/images/{{ $process->process_picture }}" download>
					<img src="global/client/img/green.png">
										</a></td>
							    <td>	<a href="/images/{{ $process->process_packaging }}" download>
					<img src="global/client/img/cloud.png">
										</a></td>
									<td><a href="/images/{{ $process->quality_inspection }}" download>
					<img src="global/client/img/pdf.png">
										</a></td>
							  </tr>
							 </table>
<!--  p>
				1.Picture :	<a href="/images/{{ $process->process_picture }}" download>
<img src="global/client/img/green.png">
					</a><br>
					2.Packaging :	<a href="/images/{{ $process->process_packaging }}" download>
	<img src="global/client/img/cloud.png">
						</a><br>
						3.Quality inspection Document:	<a href="/images/{{ $process->quality_inspection }}" download>
		<img src="global/client/img/pdf.png">
							</a><br>
<p -->
            @else
                    <h4><center>Record will be updated soon.</center></h4>
            @endif

</p>
					<span class="cd-timeline__date">
					@if(count($process) > 0)
				{{
				\Carbon\Carbon::parse($process->updated_at)->toFormattedDateString()

					}}
					@endif
					</span>
				</div>
			</div>

			<div class="cd-timeline__block js-cd-block">
				<div class="cd-timeline__img cd-timeline__img--picture js-cd-img">
					<img src="global/client/img/placeholder (3).svg" alt="Picture">
				</div>

				<div class="cd-timeline__content js-cd-content">
					<h2>Order is out for Delivery</h2>
<br>
				    @if(count($export_order) > 0)
						<h2>Export order</h2>
        <p>

        1.Balance payment method : {{  $export_order->balance_payment }} <br>
        2. Shipping Agent Co. Name : {{  $export_order->shipping_agent_name }} <br>
        3.<span style="color:red;">Balance Payment Due Date </span>: {{  $export_order->balance_due_date }}<br>
				</p>

					<table>
  <tr>
  <th> Proforma invoice</th>
    <th> Invoice </th>
    <th>Packing List</th>
    <th> Vessel Details</th>

  </tr>
  <tr>
    <td> <a href="/images/{{  $export_order->proforma_invoice }}" download> Download </a></td>
    <td> <a href="/images/{{  $export_order->invoice }}" download> Download </a></td>
		<td> <a href="/images/{{  $export_order->packing_list }}" download> Download </a></td>
		<td> <a href="/images/{{  $export_order->vessel_details }}" download> Download </a></td>

  </tr>
</table>


<br>
<table>
<tr>
<th>Bill Of Lading</th>
<th>Clearance doucuments</th>
<th> Others - Attachment</th>
</tr>
<tr>
<td> <a href="/images/{{  $export_order->bill_of_lading }}" download> Download </a></td>
<td> <a href="/images/{{  $export_order->clearance_document }}" download> Download </a></td>
<td> <a href="/images/{{  $export_order->other_document }}" download> Download </a></td>
</tr>

</table>

            @else

            @endif

<br>
            @if(count($local_order) > 0)
		<h2>Local order</h2><br>
		<p>
					1.Balance payment method : % {{  $local_order->balance_payment }} <br>
					2. Transport Company Name : {{  $local_order->company_name_transport }} <br>
					3.Transport Co. Contact Details : {{  $local_order->company_detail_transport }} <br>
					4.Freight : {{  $local_order->freight }} <br>
					3.<span style="color:red;">Balance Payment Due Date </span>: {{  $local_order->balance_due_date }}<br>
</p>


<table>
			<tr>
			<th> Proforma invoice</th>
			<th> Invoice </th>
			<th>Packing List</th>
			<th> Vessel Details</th>
    	<th>Other Docs.</th>
			</tr>
			<tr>
			<td> <a href="/images/{{  $local_order->proforma_invoice }}" download> Download </a></td>
			<td> <a href="/images/{{  $local_order->invoice }}" download> Download </a></td>
			<td> <a href="/images/{{  $local_order->packing_list }}" download> Download </a></td>
			<td> <a href="/images/{{  $local_order->lorry_receipt }}" download> Download </a></td>
			<td> <a href="/images/{{  $local_order->other_document }}" download> Download </a></td>
			</tr>
			</table>
            @else

            @endif

@if((count($local_order) == 0) && (count($export_order) == 0))
<p><h4><center>Record will be updated once order is out for delivery</center></h4><p>
@endif
					<span class="cd-timeline__date">
					@if(count($local_order) > 0)
				{{
				\Carbon\Carbon::parse($local_order->updated_at)->toFormattedDateString()

					}}
					@endif

					@if(count($export_order) > 0)
				{{
				\Carbon\Carbon::parse($export_order->updated_at)->toFormattedDateString()

					}}
					@endif

					</span>
				</div>
			</div>


	@if(count($status) > 0)
			@if($status->value == "fourth")


			<div class="cd-timeline__block js-cd-block">
				<div class="cd-timeline__img cd-timeline__img--location js-cd-img">
					<img src="global/client/img/placeholder (4).svg" alt="Location">
				</div>

				<div class="cd-timeline__content js-cd-content">
					<h2>Order Successfully Delivered</h2>
					<p>

            <br>Please fill below feedback form
            <br>
            <br>
            <br>
            <div>
            <iframe height="800" width="500" src="https://docs.google.com/forms/d/e/1FAIpQLScRUaD_jdCSQgNuMNlwMMi4r_dJ-CVTBBEQQZR4i6dXexJ6sQ/viewform?embedded=true" width="640" height="806" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
            </div>
</p>

					<span class="cd-timeline__date">
					{{
					\Carbon\Carbon::now()->parse()->toFormattedDateString()
						}}
					</span>
				</div>
			</div>
@endif

@endif
			<div class="cd-timeline__block js-cd-block">
				<div class="cd-timeline__img cd-timeline__img--location js-cd-img">
					<img src="global/client/img/placeholder (5).svg" alt="Location">
				</div>

				<div class="cd-timeline__content js-cd-content">
					<h2>Thank you.</h2>

					<span class="cd-timeline__date">

					</span>
				</div>
			</div>

			 <!-- div class="cd-timeline__block js-cd-block">
				<div class="cd-timeline__img cd-timeline__img--movie js-cd-img">
					<img src="global/client/img/cd-icon-movie.svg" alt="Movie">
				</div>

				<div class="cd-timeline__content js-cd-content">
					<h2>Final Section</h2>
					<p>This is the content of the last section</p>
					<span class="cd-timeline__date">

					</span>
				</div>
			</div -->
		</div>
	</section>

<script src="{{ asset('global/client/js/main.js') }}"></script>

<!-- <script src="global/client/js/main.js"></script> -->
 <!-- Resource JavaScript -->
</body>
</html>
