@extends('layouts.app1')

@section('heads')

@endsection
@section('content')

@section('title', 'Create Order')
@section('order', 'Create Order')
<div class="row">
    <div class="col-lg-4">
	<meta name="csrf-token" content="{{ csrf_token() }}"/>
        <form role="form" action="/create_order" method="POST">
            <div class="form-group {{ $errors->has('order_id') ? 'has-error' : '' }}">
                <label>Enter Order Id</label>
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
				 <input id="name" type="text" placeholder="Enter Order Id" class="form-control" name="order_id" value="{{ old('order_id') }}"  autofocus>
<br>
                                @if ($errors->has('order_id'))

                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('order_id') }}</strong>
                                    </p>
                                @endif


                <!-- <p class="help-block">Example block-level help text here.</p> -->
            </div>
            <button type="submit" class="btn btn-primary">Create Order</button>
            <button type="reset" class="btn btn-default">Reset</button>
        </form>
    </div>
  </div>





@endsection
