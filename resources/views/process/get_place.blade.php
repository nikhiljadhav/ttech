@extends('layouts.app1')

@section('heads')

@endsection
@section('content')

@section('title', 'Order')
@section('order', 'Place Order')
<div class="row">
    <div class="col-lg-4">
	<meta name="csrf-token" content="{{ csrf_token() }}"/>
       <form role="form" action="/post_place_order" method="POST" enctype="multipart/form-data">
            <div class="form-group {{ $errors->has('order_no') ? 'has-error' : '' }}">
                <label>Order No</label>
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
      {{Form::input('text', 'order_no', $order->order_id , ['class'=>'form-control', 'id' => 'order_no',
                                               "maxlength"=>255,"disabled"=>"true"])}}
      {{Form::input('hidden', 'order_no', $order->order_id , ['class'=>'form-control', 'id' => 'order_no',
                                                                       "maxlength"=>255])}}
								<br>
            </div>


			    <div class="form-group {{ $errors->has('order_date') ? 'has-error' : '' }}">
                <label>Enter Order Date</label>
          {{Form::input('text', 'order_date', '', ['class'=>'form-control', 'id' => 'order_date',
                                                 "placeholder"=>"Ex.2018-10-21 or 2018/1/21", "maxlength"=>255])}}
								<br>
                                @if ($errors->has('order_date'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('order_date') }}</strong>
                                    </p>
                                @endif
            </div>
			 <div class="form-group {{ $errors->has('payment_terms') ? 'has-error' : '' }}">
                <label>Select Payment Terms</label>

                  {{ Form::select('payment_terms', [
                         '' => 'Select Payment',
                         'Before Dispatch' => ' % Before Dispatch',
                         'LC At Sight' => ' %  LC At Sight',
                         '30 Days' => ' %  30 Days'
                         ],"",['class'=>'form-control',"id"=>'payment_terms']) }}
								<br>
                                @if ($errors->has('payment_terms'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('payment_terms') }}</strong>
                                    </p>
                                @endif
            </div>
			<div class="form-group {{ $errors->has('bank_details') ? 'has-error' : '' }}">
                <label>Upload Bank Details</label>
              <input type="file" name="bank_details" file-model="bank_details">
								<br>
                                @if ($errors->has('bank_details'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('bank_details') }}</strong>
                                    </p>
                                @endif
            </div>
			<div class="form-group  {{ $errors->has('dispatch_time') ? 'has-error' : '' }}">
             <label>Enter Dispatch Time</label>

             {{Form::input('text', 'dispatch_time', '', ['class'=>'form-control', 'id' => 'dispatch_time',
                  "placeholder"=>"Dispatch Time", "maxlength"=>255])}}
                @if ($errors->has('dispatch_time'))
                <p class="text-danger" role="alert">
                <strong>{{ $errors->first('dispatch_time') }}</strong>
                </p>
                @endif
             </div>



			  {{ Form::submit('Place Order', ['class'=>'btn btn-primary']) }}
</form>
      <!-- {{ Form::close() }} -->
    </div>
  </div>





@endsection
