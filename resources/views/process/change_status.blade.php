@extends('layouts.app1')

@section('heads')

@endsection
@section('content')

@section('title', 'Order')
@section('order', 'Change Order Status')
<div class="row">
    <div class="col-lg-4">
	<meta name="csrf-token" content="{{ csrf_token() }}"/>
       <form role="form" action="/change_status" method="POST" enctype="multipart/form-data">
            <div class="form-group {{ $errors->has('order_no') ? 'has-error' : '' }}">
                <label>Order No</label>
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
      {{Form::input('text', 'order_no', $order->order_id , ['class'=>'form-control', 'id' => 'order_no',
                                                 "maxlength"=>255,"disabled"=>"true"])}}
        {{Form::input('hidden', 'order_no', $order->order_id , ['class'=>'form-control', 'id' => 'order_no',
                                                                         "maxlength"=>255])}}
								<br>
            </div>


			    <div class="form-group {{ $errors->has('order_status') ? 'has-error' : '' }}">
                <label>Upload Pictures</label>

                <select class="form-control" name="order_status" id="gender">
                                          <option value="">Select Status</option>
                                      <option value="first" {{ 'first' === $order->order_status ? 'selected' : '' }}>Order Placed</option>
                                      <option value="second" {{ 'second' === $order->order_status ? 'selected' : '' }}>Order is in Process</option>
                                      <option value="third" {{ 'third' === $order->order_status ? 'selected' : '' }}>Out For Delivery</option>
                                      <option value="fourth" {{ 'fourth' === $order->order_status ? 'selected' : '' }}>Delivered</option>
                                      </select>


           <!-- {{ Form::select('order_status', [
                              '' => 'Select Status',
                              'first' => 'Order Placed',
                              'second' => 'Out For Delivery',
                              'third' => 'Shipped',
                              'fourth' => 'Delivered'
                              ],"",['class'=>'form-control',"id"=>'order_status']) }} -->
								<br>
                                @if ($errors->has('order_status'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('order_status') }}</strong>
                                    </p>
                                @endif
            </div>
			 <input name="_method" type="hidden" value="PUT">
			<input type="hidden" name="id" value="{{ $order->id }}">
			  {{ Form::submit('Change Status', ['class'=>'btn btn-primary']) }}
</form>
    </div>
  </div>





@endsection
