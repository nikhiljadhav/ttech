@extends('layouts.app1')

@section('heads')

@endsection
@section('content')

@section('title', 'Order')
@section('order', 'Process Order')
<div class="row">
    <div class="col-lg-4">
	<meta name="csrf-token" content="{{ csrf_token() }}"/>
       <form role="form" action="/post_process_order" method="POST" enctype="multipart/form-data">
            <div class="form-group {{ $errors->has('order_no') ? 'has-error' : '' }}">
                <label>Order No</label>
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
     {{Form::input('text', 'order_no',$order->order_id , ['class'=>'form-control', 'id' => 'order_no', "maxlength"=>100,"disabled"=>"true"])}}
                        {{Form::input('hidden', 'order_no',$order->order_id , ['class'=>'form-control', 'id' => 'order_no',"maxlength"=>100])}}
								<br>
            </div>


			    <div class="form-group {{ $errors->has('process_picture') ? 'has-error' : '' }}">
                <label>Upload Pictures</label>
           <input type="file" name="process_picture" file-model="process_picture">
								<br>
                                @if ($errors->has('process_picture'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('process_picture') }}</strong>
                                    </p>
                                @endif
            </div>

			<div class="form-group {{ $errors->has('process_packaging') ? 'has-error' : '' }}">
                <label>Upload Packaging Attachment</label>
               <input type="file" name="process_packaging" file-model="process_packaging">
								<br>
                                @if ($errors->has('process_packaging'))
                                    <p class="text-danger" role="alert">
                                        <strong>{{ $errors->first('process_packaging') }}</strong>
                                    </p>
                                @endif
            </div>
			<div class="form-group  {{ $errors->has('quality_inspection') ? 'has-error' : '' }}">
             <label>Upload Quality Inspection Attachment</label>

              <input type="file" name="quality_inspection" file-model="quality_inspection">
                @if ($errors->has('quality_inspection'))
                <p class="text-danger" role="alert">
                <strong>{{ $errors->first('quality_inspection') }}</strong>
                </p>
                @endif
             </div>



			  {{ Form::submit('Submit', ['class'=>'btn btn-primary']) }}
</form>
      <!-- {{ Form::close() }} -->
    </div>
  </div>





@endsection
